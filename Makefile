cc = g++ -std=c++2a -O2 -pipe -lstdc++fs
name = hypescript

done_print = \e[1;34mbuild done\e[0m
linking_print = \e[1;32m☍  linking\e[0m
compile_print = \e[1;33mcompiling\e[0m

files_and_dirs := $(wildcard src/*) $(wildcard src/*/*) $(wildcard src/*/*/*)
files := $(filter %.cpp, $(files_and_dirs))

targets = $(basename $(notdir $(files)))
build_list = $(addprefix build/, $(addsuffix .o, $(targets)))

link:
	@echo "$(linking_print)"
	@$(cc) $(build_list) $(libs) -o $(name)
	@echo "$(done_print)"

all: $(targets)
	@echo "$(linking_print)"
	@$(cc) $(build_list) $(libs) -o $(name)
	@echo "$(done_print)"

%:
	@echo "$(compile_print) $@"
	@$(cc) -c $(shell find src -name $@.cpp) -o build/$@.o



