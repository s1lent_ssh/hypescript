#pragma once

namespace hypescript {
    enum class expression_type {
        _int,
        _double,
        _string,
        _bool,
        _void,
        invalid
    };
}