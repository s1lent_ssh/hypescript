#pragma once

#include "expression.h"
#include <iostream>

namespace hypescript {

class invalid_expression : public expression {
public:
    invalid_expression();

    std::any operator()();

    void debug(const std::string& prefix, bool isLeft);
};

}