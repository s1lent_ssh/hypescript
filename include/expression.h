#pragma once

#include <memory>
#include "expression_type.h"
#include <any>
#include <iostream>

namespace hypescript {

class expression {
    
protected:
    expression_type _type;

public:
    expression(expression_type type);

    virtual std::any operator()() = 0;

    virtual void debug(const std::string& prefix, bool isLeft) = 0;
    void debug();

    void set_type(expression_type type);
    expression_type type();
};

typedef std::unique_ptr<expression> expression_ptr;

}