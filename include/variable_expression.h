#pragma once
#include "expression.h"
#include "builtin_functions.h"

namespace hypescript {
class variable_expression : public expression {
    const std::string       _name;
public:
    variable_expression(const std::string& name);

    void debug(const std::string& prefix, bool isLeft);
    std::any operator()();
};

}