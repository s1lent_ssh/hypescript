#pragma once
#include "expression.h"
#include "operation_type.h"
#include <iostream>

namespace hypescript {

class binary_expression : public expression {
    operation_type _operation;
    expression_ptr _lhs;
    expression_ptr _rhs;
public:
    binary_expression(expression_ptr&& lhs, expression_ptr&& rhs, operation_type operation, expression_type type);
    std::any operator()();
    void debug(const std::string& prefix, bool isLeft);
};

}
