#pragma once
#include <map>
#include <functional>
#include <any>
#include <cmath>
#include "interpreter.h"

namespace hypescript {
class builtin_functions {

    std::map<std::string, std::function<double(double)>> _functions_list {
        {"sin", sin},
        {"cos", cos},
        {"tan", tan},
        {"acos", acos},
        {"asin", asin},
        {"atan", atan},
        {"cosh", cosh},
        {"sinh", sinh},
        {"acosh", acosh},
        {"atanh", atanh},
        {"exp", exp},
        {"log", log},
        {"log10", log10}
    };

public:
    std::any call(std::string name, std::any arg);
};
}