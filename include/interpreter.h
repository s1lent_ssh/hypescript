#pragma once
#include <filesystem>
#include <fstream>
#include "parser.h"

namespace hypescript {

class interpreter {
    const std::string _path;
    static std::map<std::string, std::any> _variables;
public:
    interpreter(const std::string& path);

    static std::any get(const std::string& name);

    static void set(const std::string& name, const std::any& value);

    void operator()();
};

}