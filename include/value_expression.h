#pragma once

#include "expression.h"
#include <iostream>
#include <any>
#include "builtin_functions.h"
#include "expression_type.h"
#include "interpreter.h"

namespace hypescript {

class value_expression : public expression {
    std::any _value;
    
public:
    value_expression(std::string value, expression_type type);

    std::any operator()();

    void debug(const std::string& prefix, bool isLeft);
    void print();
};

}