#pragma once
#include "expression.h"
#include "builtin_functions.h"

namespace hypescript {
class function_expression : public expression {
    const std::string       _name;
    const expression_ptr    _expression;
public:
    function_expression(const std::string& name, expression_ptr&& args);
    void debug(const std::string& prefix, bool isLeft);
    std::any operator()();
};

}