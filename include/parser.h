#pragma once

#include "expression.h"
#include <iostream>
#include <regex>
#include <iomanip> 
#include <limits>
#include <functional>
#include "operation_type.h"
#include "value_expression.h"
#include "binary_expression.h"
#include "invalid_expression.h"
#include "function_expression.h"
#include "variable_expression.h"

namespace hypescript {

typedef std::unique_ptr<expression> expression_ptr;

class parser {

    std::map<char, std::pair<uint8_t, operation_type>> _operators {
        {'+', {50, operation_type::add}},
        {'-', {50, operation_type::substract}},
        {'*', {100, operation_type::multiply}},
        {'/', {100, operation_type::divide}},
        {'<', {30, operation_type::less}},
        {'>', {30, operation_type::more}}
    };

    std::regex _variable_pattern = std::regex("([a-zA-z]+)");

    std::vector<std::pair<std::regex, expression_type>> regex_types {
        {std::regex("(true|false)"),                                                                  expression_type::_bool},
        {std::regex("(\\+|-)?[[:d:]]+"),                                                              expression_type::_int},
        {std::regex("((\\+|-)?[[:digit:]]+)\\.((([[:digit:]]+)?))?((e|E)((\\+|-)?)[[:digit:]]+)?"),   expression_type::_double},
        {std::regex("(\"[^\"]*\")"),                                                                  expression_type::_string},
    };

    size_t find_close_bracket(const std::string& data, size_t start);

public:

    expression_type parse_expression_type(const std::string& data);
    expression_ptr parse_expression(const std::string& _data);
    operation_type parse_operation_type(char data);
    std::pair<size_t, size_t> find_split_bound(const std::string& data);
    std::string remove_whitespaces(std::string data);

};

}