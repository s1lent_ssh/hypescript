#pragma once

namespace hypescript {

enum class operation_type {
    add,
    divide,
    substract,
    multiply,
    more,
    less
};

}