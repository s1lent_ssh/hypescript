#include "../include/expression.h"

namespace hypescript {

expression::expression(expression_type type) : _type(type) {}

expression_type expression::type() {
    return _type;
}

void expression::set_type(expression_type type) {
    _type = type;
}

void expression::debug() {
    auto result = this->operator()();
    switch(this->_type) {
        case expression_type::_bool:
            std::cout << std::any_cast<bool>(result) << std::endl; 
            break;
        case expression_type::_int:
            std::cout << std::any_cast<int>(result) << std::endl; 
            break;
        case expression_type::_double:
            std::cout << std::any_cast<double>(result) << std::endl; 
            break;
        case expression_type::_string:
            std::cout << std::any_cast<std::string>(result) << std::endl; 
            break;
        case expression_type::_void:
            std::cout << "void() executed" << std::endl; 
            break;
    }
    this->debug("", false);
    std::cout << std::endl;
}

}