#include "../include/invalid_expression.h"

namespace hypescript {

invalid_expression::invalid_expression() : expression(expression_type::invalid) {}

std::any invalid_expression::operator()() {
    return 0;
}
void invalid_expression::debug(const std::string& prefix, bool isLeft) {
    std::cout << "It's invalid" << std::endl;
}

}