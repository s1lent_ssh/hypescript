#include "../include/value_expression.h"

namespace hypescript {

value_expression::value_expression(std::string value, expression_type type) : expression(type) {
    switch(type) {
        case expression_type::_int:
            _value = std::stoi(value);
            break;
        case expression_type::_double:
            _value = std::stod(value);
            break;
        case expression_type::_string:
            _value = value.substr(1, value.size() - 2);
            break;
    }
}

std::any value_expression::operator()() {
    return _value;
}

void value_expression::debug(const std::string& prefix, bool isLeft) {
    std::cout << prefix;
    std::cout << (isLeft ? "├──" : "└──" );
    switch(_type) {
        case expression_type::_int:
            std::cout << std::any_cast<int>(_value) << std::endl;
            break;
        case expression_type::_double:
            std::cout << std::any_cast<double>(_value) << std::endl;
            break;
        case expression_type::_string:
            std::cout << std::any_cast<std::string>(_value) << std::endl;
            break;
    }
}

}