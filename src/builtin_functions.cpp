#include "../include/builtin_functions.h"

namespace hypescript {
void print(std::any parameter) {
    if(parameter.type() == typeid(std::string)) {
        std::cout << std::any_cast<std::string>(parameter) << std::endl;
    } else if(parameter.type() == typeid(int)) {
        std::cout << std::any_cast<int>(parameter) << std::endl;
    } else if(parameter.type() == typeid(double)) {
        std::cout << std::any_cast<double>(parameter) << std::endl;
    } else if(parameter.type() == typeid(bool)) {
        std::cout << std::any_cast<bool>(parameter) << std::endl;
    }
}

std::any builtin_functions::call(std::string name, std::any parameter) {
    if(_functions_list.find(name) != _functions_list.end()) {
        if(parameter.type() == typeid(double)) {
            return _functions_list.at(name)(std::any_cast<double>(parameter));
        } else {
            return 1;
        }
    } else if (name == "print") {
        print(parameter);
        return 0;
    } else {
        std::cout << "Call function: " << name << " error." << std::endl;
        return 1;
    }
}
}