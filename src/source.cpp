#include "../include/parser.h"
#include "../include/expression_type.h"
#include "../include/interpreter.h"

auto main(int argc, const char* argv[]) -> int {
    std::cout << std::boolalpha;

    if(argc != 2) {
        std::cerr << "No source code passed" << std::endl;
        return 1;
    }

    try {
        (hypescript::interpreter(argv[1]))();
    } catch(const std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        return 1;
    }

}