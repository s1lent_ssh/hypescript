#include "../include/variable_expression.h"

namespace hypescript {

variable_expression::variable_expression(const std::string& name) : 
        expression(expression_type::invalid), 
        _name(name) {
            auto value = interpreter::get(name);
            if(value.type() == typeid(int)) {
                set_type(expression_type::_int);
            } else if(value.type() == typeid(double)) {
                set_type(expression_type::_double);
            } else if(value.type() == typeid(std::string)) {
                set_type(expression_type::_string);
            } else if(value.type() == typeid(bool)) {
                set_type(expression_type::_bool);
            } 
        }

void variable_expression::debug(const std::string& prefix, bool isLeft) {
    std::cout << prefix;
    std::cout << (isLeft ? "├──" : "└──" );
    auto value = interpreter::get(_name);
    switch(_type) {
        case expression_type::_int:
            std::cout << std::any_cast<int>(value) << std::endl;
            break;
        case expression_type::_double:
            std::cout << std::any_cast<double>(value) << std::endl;
            break;
        case expression_type::_bool:
            std::cout << std::any_cast<bool>(value) << std::endl;
            break;
        case expression_type::_string:
            std::cout << std::any_cast<std::string>(value) << std::endl;
            break;
    }
}

std::any variable_expression::operator()() {
    return interpreter::get(_name);
}

}