#include "../include/binary_expression.h"

namespace hypescript {

binary_expression::binary_expression(
    expression_ptr&& lhs, 
    expression_ptr&& rhs, 
    operation_type operation, 
    expression_type type
) : _lhs(std::move(lhs)), 
    _rhs(std::move(rhs)), 
    _operation(operation), 
    expression(type) {}

template<class T>
bool operate_bool(T lhs, T rhs, operation_type operation) {
    switch (operation) {
        case operation_type::less:
            return lhs < rhs;
        case operation_type::more:
            return lhs > rhs;  
    }
    std::cout << "Bool operation error" << std::endl;
    return false;
}

template<typename T>
T operate_numeric(T lhs, T rhs, operation_type operation) {
    switch (operation) {
        case operation_type::add:
            return lhs + rhs;
        case operation_type::substract:
            return lhs - rhs;
        case operation_type::multiply:
            return lhs * rhs;
        case operation_type::divide:
            return lhs / rhs;
        case operation_type::less:
            return lhs < rhs;
        case operation_type::more:
            return lhs > rhs;  
    }
    std::cout << "Int operation error" << std::endl;
    return 0;
}

std::string operate_string(std::string lhs, std::string rhs, operation_type operation) {
    switch (operation) {
        case operation_type::add:
            return lhs + rhs;
    }
    std::cout << "String operation error" << std::endl;
    return "";
}

std::any binary_expression::operator()() {

    switch(this->type()) {
        case expression_type::_double: {
            auto    lhs = std::any_cast<double>((*_lhs)()),
                    rhs = std::any_cast<double>((*_rhs)());
            return operate_numeric(lhs, rhs, _operation); 
        }
        case expression_type::_int: {
            auto    lhs = std::any_cast<int>((*_lhs)()),
                    rhs = std::any_cast<int>((*_rhs)());
            return operate_numeric(lhs, rhs, _operation); 
        }
        case expression_type::_string: {
            auto    lhs = std::any_cast<std::string>((*_lhs)()),
                    rhs = std::any_cast<std::string>((*_rhs)());
            return operate_string(lhs, rhs, _operation);
        }
        case expression_type::_bool: {
            switch(_lhs->type()) {
                case expression_type::_int: {
                    auto    lhs = std::any_cast<int>((*_lhs)()),
                            rhs = std::any_cast<int>((*_rhs)());
                    return operate_bool(lhs, rhs, _operation);
                }
                case expression_type::_double: {
                    auto    lhs = std::any_cast<double>((*_lhs)()),
                            rhs = std::any_cast<double>((*_rhs)());
                    return operate_bool(lhs, rhs, _operation);
                }
            }
        }
    }
    std::cout << "Binary expression don't dupport this type" << std::endl;
    return 0;
}

void binary_expression::debug(const std::string& prefix, bool isLeft) {
    std::cout << prefix;
    std::cout << (isLeft ? "├──" : "└──" );
    switch (_operation) {
        case operation_type::add:
            std::cout << " +" << std::endl;
            break;
        case operation_type::substract:
            std::cout << " -" << std::endl;
            break;
        case operation_type::multiply:
            std::cout << " *" << std::endl;
            break;
        case operation_type::divide:
            std::cout << " /" << std::endl;
            break;
        case operation_type::less:
            std::cout << " <" << std::endl;
            break;
        case operation_type::more:
            std::cout << " >" << std::endl;
            break;
        default:
            std::cout << "No print implementation for operation" << std::endl;
    }
    _lhs->debug( prefix + (isLeft ? "│   " : "    "), true);
    _rhs->debug( prefix + (isLeft ? "│   " : "    "), false);
}

}