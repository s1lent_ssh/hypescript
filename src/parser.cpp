#include "../include/parser.h"

namespace hypescript {

expression_type parser::parse_expression_type(const std::string& data) {
    for(const auto& [pattern, type] : regex_types) {
        if(std::regex_match(data, pattern)) {
            return type;
        }
    }
    return expression_type::invalid;
}

expression_ptr parser::parse_expression(const std::string& _data) {
    std::string data = remove_whitespaces(_data);
    //Its maybe function
    if(data.back() == ')' && std::isalpha(data.at(0)) && find_close_bracket(data, 0) == data.size() - 1) {
        auto open_brace = data.find('(');
        std::string name = data.substr(0, open_brace);
        bool valid_name = std::all_of(name.begin(), name.end(), [](char c){ return std::isdigit(c) || std::isalpha(c); }); 
        if(valid_name) {
            //Its a function
            std::string args = data.substr(open_brace + 1, data.size() - open_brace - 2);
            auto&& args_expression = parse_expression(args);
            return std::make_unique<function_expression>(name, std::move(args_expression));
        }
    }
    //Its value
    for(const auto& [pattern, type] : regex_types) {
        if(std::regex_match(data, pattern)) {
            return std::make_unique<value_expression>(data, type);
        }
    }
    //Its variable
    if(std::regex_match(data, _variable_pattern)) {
        return std::make_unique<variable_expression>(data);
    }

    //Its binary or invalid
    const auto[padding, middle] = find_split_bound(data);
    auto&& lhs = parse_expression(data.substr(padding, middle - padding));
    auto&& rhs = parse_expression(data.substr(middle + 1, data.size() - middle - padding * 2));
    if(lhs->type() != rhs->type()) {
        return std::make_unique<invalid_expression>();
    } else {
        if(auto type = parse_operation_type(data.at(middle)); type == operation_type::less || type == operation_type::more) {
            return std::make_unique<binary_expression>(std::move(lhs), std::move(rhs), type, expression_type::_bool);
        } else {
            return std::make_unique<binary_expression>(std::move(lhs), std::move(rhs), type, lhs->type());
        }
    }
}
operation_type parser::parse_operation_type(char data) {
    return _operators.at(data).second;
}
std::pair<size_t, size_t> parser::find_split_bound(const std::string& data) {
    size_t  min_operator_position {0};
    int     min_operator_priority {std::numeric_limits<int>::max()};

    //Check for whole brace expression -> (...)
    size_t  padding {0};
    if(data.at(0) == '(' && find_close_bracket(data, 0) == data.size() - 1) {
        if(data.at(data.size() - 1) != ')') {
            throw std::runtime_error("No close brace");
        }
        padding = 1;
    }
    //End of check

    for(size_t i = padding; i < data.size() - padding; i++) {
        if(data.at(i) == '(') {
            auto close_pos = find_close_bracket(data, i);
            if(close_pos == data.size() - 1 - padding) {
                break;
            } else {
                i = close_pos;
            }
        }
        if(_operators.count(data.at(i)) > 0) {
            if(int priority = _operators.at(data.at(i)).first; priority <= min_operator_priority) {
                min_operator_priority = priority;
                min_operator_position = i;
            }
        }
    }
    return {padding, min_operator_position};
}
std::string parser::remove_whitespaces(std::string data) {
    bool is_string_now = false;
    data.erase(std::remove_if(data.begin(), data.end(), [&](char c) {
        if(c == '\"') {
            is_string_now = !is_string_now;
        }
        if(is_string_now) {
            return 0;
        } else {
            return std::isspace(c);
        }
    }), data.end());
    return data;
}

size_t parser::find_close_bracket(const std::string& data, size_t start) {
    size_t brackets_counter {0};
    for(int i = start; i != data.size(); i++) {
        if(data.at(i) == ')') {
            brackets_counter--;
            if(brackets_counter == 0) {
                return i;
            }
        } else if(data.at(i) == '(') {
            brackets_counter++;
        }
    }
    return 0;
}

}