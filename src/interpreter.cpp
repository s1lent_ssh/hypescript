#include "../include/interpreter.h"

namespace hypescript {

std::map<std::string, std::any> interpreter::_variables = std::map<std::string, std::any>();

interpreter::interpreter(const std::string& path) : _path(path) {}

std::any interpreter::get(const std::string& name) {
    return _variables.at(name);
}

void interpreter::set(const std::string& name, const std::any& value) {
    _variables[name] = value;
}

void interpreter::operator()() {
    std::ifstream file(_path);
    if(file.is_open()) {
        auto parser = hypescript::parser();
        std::string line;
        while(std::getline(file, line)) {
            if(line.empty()) {
                continue;
            } else if(line.size() > 3 && line.substr(0, 3) == "let") {
                auto equals_aign = line.find('=');
                std::string name = line.substr(4, equals_aign - 5);
                std::string expression = line.substr(equals_aign + 2, line.size() - equals_aign);
                auto ex = parser.parse_expression(expression);
                ex->debug();
                interpreter::set(name, (*ex)());
            } else if(line.size() > 2 && line.substr(0, 2) == "if") {
                std::string expression = line.substr(3, line.size() - 3);
                auto ex = parser.parse_expression(expression);
            } else {
                auto ex = parser.parse_expression(line);
                ex->debug();
            }
        }
    }
}

}