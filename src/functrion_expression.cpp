#include "../include/function_expression.h"

namespace hypescript {

function_expression::function_expression(const std::string& name, expression_ptr&& args) : 
        expression(args->type()), 
        _name(name), 
        _expression(std::move(args)) {
            if(name == "print") {
                this->set_type(expression_type::_void);
            }
        }

void function_expression::debug(const std::string& prefix, bool isLeft) {
    std::cout << prefix;
    std::cout << (isLeft ? "├──" : "└──" );
    std::cout << _name << "()" << std::endl;
    _expression->debug( prefix + (isLeft ? "│   " : "    "), false);
}
std::any function_expression::operator()() {
    return builtin_functions().call(_name, (*_expression)());
}

}